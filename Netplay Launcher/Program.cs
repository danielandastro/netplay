﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Xaml;

namespace Netplay_Launcher
{
    
    class Program
    {
[STAThread]
        static void Main(string[] args)
        {
            var player = new System.Windows.Application();
            var name = "";
            try { name = args[0]; }
            catch(Exception ex) { }

            if (string.IsNullOrEmpty(name))
            {
                
                player.Run(new NetPlay.MainWindow());
            }
            else
            {
                player.Run(new MiniPlayer.MainWindow(name));
            }

        }
    }
}
