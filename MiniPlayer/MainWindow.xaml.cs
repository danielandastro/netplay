﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NAudio;
using NAudio.Wave;
namespace MiniPlayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CancellationTokenSource token = new CancellationTokenSource();
        CancellationTokenSource pause = new CancellationTokenSource();
        CancellationTokenSource playToken = new CancellationTokenSource();
        public MainWindow(string fileName)
        {
            InitializeComponent();
            play(fileName, token.Token, pause.Token, playToken.Token);
            playBTN.IsEnabled = false;
            fileNameLBL.Content = fileName;
        }
        public async void play(string fileName, CancellationToken cancellationToken, CancellationToken pauseToken, CancellationToken resumeToken)
        {
            using (var audioFile = new AudioFileReader(fileName))
            using (var outputDevice = new WaveOutEvent())
            {
                outputDevice.DeviceNumber = -1;
                outputDevice.Init(audioFile);
                outputDevice.Play();
                int currentMillis = 0;
                
                while (outputDevice.PlaybackState == PlaybackState.Playing || outputDevice.PlaybackState == PlaybackState.Paused)
                {
                    if (cancellationToken.IsCancellationRequested) { break; }
                    if (pauseToken.IsCancellationRequested) { outputDevice.Pause(); }
                    if (resumeToken.IsCancellationRequested) { outputDevice.Play(); }
                    
                    Thread.Sleep(1000);

                }
            }
            return;
        }
        private void PlayBTN_Click(object sender, RoutedEventArgs e)
        {
            playToken.Cancel();
            playToken.Dispose();
            pauseBTN.IsEnabled = true;
            playToken = new CancellationTokenSource();
        }

        private void StopBTN_Click(object sender, RoutedEventArgs e)
        {
            token.Cancel();
            return;
        }

        private void PauseBTN_Click(object sender, RoutedEventArgs e)
        {
            pause.Cancel();
            pause.Dispose();
            playBTN.IsEnabled = true;
            pause = new CancellationTokenSource();
        }
    }
}
