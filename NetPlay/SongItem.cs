﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetPlay
{
    public struct songItem
    {
        public int id;
        public string title
        { get; set; }
        public string fileName;
        public string artist;
        public string album;
        public int length;
        public MemoryStream albumArt;
    }
}
