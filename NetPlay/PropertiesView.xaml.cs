﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NetPlay
{
    /// <summary>
    /// Interaction logic for PropertiesView.xaml
    /// </summary>
    public partial class PropertiesView : Window
    {
        public PropertiesView(songItem item)
        {
            InitializeComponent();
            propertiesListBox.Items.Add("Title: " + item.title);
            propertiesListBox.Items.Add("File Name: " + item.fileName);
            propertiesListBox.Items.Add("Artist: " + item.artist);
            propertiesListBox.Items.Add("Album: " + item.album);
            TimeSpan t = TimeSpan.FromMilliseconds(item.length);
            string length = string.Format("{0:D2}h:{1:D2}m:{2:D2}s:{3:D3}ms",
                                    t.Hours,
                                    t.Minutes,
                                    t.Seconds,
                                    t.Milliseconds);
            propertiesListBox.Items.Add("Length " + length);
        }
    }
}
