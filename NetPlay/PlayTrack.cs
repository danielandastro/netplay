﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using NAudio;
using NAudio.Wave;

namespace NetPlay
{
    partial class MainWindow
    {
        public WaveOutEvent output = new WaveOutEvent();
        public async void playTrack(songItem trackToPlay)
        {
            using (var audioFile = new AudioFileReader(trackToPlay.fileName))
            {
                this.Dispatcher.Invoke(() =>
                {
                    trackBox.Content = trackToPlay.title;
                    albumBox.Content = trackToPlay.album;
                    artistBox.Content = trackToPlay.artist;
                    singleRDB.IsEnabled = false;
                    allRDB.IsEnabled = false;
                    playBTN.IsEnabled = false;
                    stpBTN.IsEnabled = true;
                    pauseBTN.IsEnabled = true;
                    outBox.IsEnabled= false;
                    fileListBox.SelectedItem=trackToPlay;
                    try
                    {
                        trackToPlay.albumArt.Seek(0, SeekOrigin.Begin);
                        var imageSource = new BitmapImage();
                        imageSource.BeginInit();
                        imageSource.StreamSource = trackToPlay.albumArt;
                        imageSource.EndInit();
                        albumArtBox.Source = imageSource;
                    }
                    catch (Exception) {
                        albumArtBox.Source = null;
                    }
                });
                
                output.DesiredLatency = Properties.Settings.Default.bufferTime;
                output.Init(audioFile);
                output.Play();
                int currentMillis = 0;
                int trackMillis = trackToPlay.length;
                System.Diagnostics.Debug.WriteLine(trackMillis);
                playProgress.Dispatcher.Invoke(() => playProgress.Maximum = trackToPlay.length, System.Windows.Threading.DispatcherPriority.Background);
                while (output.PlaybackState == PlaybackState.Playing || output.PlaybackState == PlaybackState.Paused)
                {
                    currentMillis = (int)((output.GetPosition() / output.OutputWaveFormat.AverageBytesPerSecond)*1000);
                    playProgress.Dispatcher.Invoke(() => playProgress.Value = currentMillis, System.Windows.Threading.DispatcherPriority.Background);
                    System.Diagnostics.Debug.WriteLine(trackMillis);
                    completedLabel.Dispatcher.Invoke(() => completedLabel.Content = returnTimeStamp(currentMillis));
                    remainingLabel.Dispatcher.Invoke(() => remainingLabel.Content = returnTimeStamp((int)playProgress.Maximum - currentMillis));
                    //Thread.Sleep(Properties.Settings.Default.loopTime);

                }
   
                this.Dispatcher.Invoke(() =>
                {
                    trackBox.Content = "";
                    albumBox.Content = "";
                    artistBox.Content = "";
                    singleRDB.IsEnabled = true;
                    allRDB.IsEnabled = true;
                    playBTN.IsEnabled = true;
                    stpBTN.IsEnabled = false;
                    pauseBTN.IsEnabled = false;
                    outBox.IsEnabled = true;
                    albumArtBox.Source = null;
                });
            }

            return;
        }

        public string returnTimeStamp(int millis)
        {
            TimeSpan t = TimeSpan.FromMilliseconds(millis);
            string length = string.Format("{0:D2}:{1:D2}:{2:D2}",
                        t.Hours,
                        t.Minutes,
                        t.Seconds);
            return length;
        }
    }
}
