﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NetPlay
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            InitializeComponent();
            bufferValBOX.Text = Properties.Settings.Default.bufferTime.ToString();
            loopValBOX.Text = Properties.Settings.Default.loopTime.ToString();
            showDataChkBx.IsChecked = Properties.Settings.Default.showData;
            fldrMdeBox.IsChecked = Properties.Settings.Default.sinMDE;
            scanSubDirsChk.IsChecked = Properties.Settings.Default.scanSubDirs;
            rememberFormat.IsChecked = Properties.Settings.Default.remeberFormat;
            defaultFormatBox.Text = Properties.Settings.Default.defaultFormat;
            defaultFormatBox.IsEnabled = (bool)!rememberFormat.IsChecked;
        }

        private void ResetBtn_Click(object sender, RoutedEventArgs e)
        {
            bufferValBOX.Text = "1000";
            loopValBOX.Text = "500";
            showDataChkBx.IsChecked = false;
            fldrMdeBox.IsChecked = false;
        }

        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Properties.Settings.Default.bufferTime = Convert.ToInt32(bufferValBOX.Text);
                Properties.Settings.Default.loopTime = Convert.ToInt32(loopValBOX.Text);
                Properties.Settings.Default.showData = (bool)showDataChkBx.IsChecked;
                Properties.Settings.Default.sinMDE = (bool)fldrMdeBox.IsChecked;
                Properties.Settings.Default.scanSubDirs = (bool)scanSubDirsChk.IsChecked;
                Properties.Settings.Default.remeberFormat = (bool)rememberFormat.IsChecked;
                Properties.Settings.Default.defaultFormat = defaultFormatBox.Text;

            }
            catch (Exception)
            {
                alertdata.Content = "Please ensure your entries are valid";
                return;
            }
            Properties.Settings.Default.Save();
        }

        private void rememberFormat_Checked(object sender, RoutedEventArgs e)
        {
            defaultFormatBox.IsEnabled = (bool)!rememberFormat.IsChecked;
        }

        private void scanSubDirsChk_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool)scanSubDirsChk.IsChecked) { MessageBox.Show("Warning: May cause significant performance loss"); }
        }
    }
}
