﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;
using NAudio.Wave;
using System.Drawing;

namespace NetPlay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CancellationTokenSource token = new CancellationTokenSource();
        CancellationTokenSource skip = new CancellationTokenSource();
        CancellationTokenSource pause = new CancellationTokenSource();
        CancellationTokenSource playToken = new CancellationTokenSource();
        List<songItem> listOfSongs = new List<songItem>();
        List<string> outputs = new List<string>();
        bool single = false;
        public bool isPaused = false;
        public bool isStopped = false;
        public MainWindow()
        {
            InitializeComponent();
            initApp();

            void initApp()
            {
                for (int n = -1; n < WaveOut.DeviceCount; n++)
                {
                    var caps = WaveOut.GetCapabilities(n);
                    outputs.Add($"{caps.ProductName}");
                }
                outBox.ItemsSource = outputs;
                try
                {
                    outBox.SelectedIndex = Properties.Settings.Default.lastOutput;

                }
                catch (System.Exception)
                {
                    outBox.SelectedIndex = 0;
                }
                output.DeviceNumber = outBox.SelectedIndex - 1;
                singleRDB.IsChecked = Properties.Settings.Default.sinMDE;
                allRDB.IsChecked = !Properties.Settings.Default.sinMDE;
                Directory.SetCurrentDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic));
                dirBox.Text = Directory.GetCurrentDirectory();
                System.Diagnostics.Debug.WriteLine(Properties.Settings.Default.lastFormat);
                if (Properties.Settings.Default.remeberFormat) { extnBox.Text = Properties.Settings.Default.lastFormat; }
                else { extnBox.Text = Properties.Settings.Default.defaultFormat; }
                System.Diagnostics.Debug.WriteLine(Properties.Settings.Default.lastFormat);
                scanFiles("." + extnBox.Text);
            }
        }
        private async void scanFiles(string extension)
        {
            listOfSongs.Clear();
            songItem temp = new songItem();
            var fileList = new string[1];
            if (Properties.Settings.Default.scanSubDirs) { fileList = Directory.GetFiles(Directory.GetCurrentDirectory(), "*" + extension, SearchOption.AllDirectories); }
            else { fileList = Directory.GetFiles(Directory.GetCurrentDirectory(), "*" + extension); }
            int id = 0;
            foreach (string name in fileList)
            {
                try
                {
                    temp.id = id++;
                    var tfile = TagLib.File.Create(name);
                    temp.albumArt = new MemoryStream();
                    temp.title = tfile.Tag.Title;
                    temp.album = tfile.Tag.Album;
                    temp.artist = tfile.Tag.Performers[0];
                    temp.fileName = name;
                    temp.length = Convert.ToInt32(tfile.Properties.Duration.TotalMilliseconds);
                    var firstPicture = tfile.Tag.Pictures.FirstOrDefault();
                    if (firstPicture != null)
                    {
                        byte[] pData = firstPicture.Data.Data;
                        temp.albumArt.Write(pData, 0, Convert.ToInt32(pData.Length));
                    }
                    string addString = "";
                    addString = temp.title;
                    if (Properties.Settings.Default.showData) addString += "/" + temp.album + "/" + temp.artist;
                    listOfSongs.Add(temp);
                    //fileListBox.DataContext = this;
                    fileListBox.SelectedIndex = 0;
                }
                catch(Exception e)
                {
                    statusBOX.Content = "One or more files in this folder were corrupted or invalid, playbck will continue for usable files, corrupted files may lead to performance losses";
                }
                fileListBox.ItemsSource = listOfSongs;
            }
        }

        private async void PlayBTN_Click(object sender, RoutedEventArgs e)
        {
            if (isPaused == true)
            {
                pauseBTN.IsEnabled = true;
                output.Play();
                isPaused = false;
                playBTN.IsEnabled = false;
                return;
            }
            else { playControls(); }
        }
        
        private async void playControls()

            {
                if (!single)
                {
                    isStopped = false;
                    while (fileListBox.SelectedIndex < listOfSongs.Count)
                    {
                        if (isStopped) { break; }
                        var item = fileListBox.SelectedItem;
                        await Task.Run(() => playTrack((songItem)item));
                        fileListBox.SelectedIndex++;
                    }
                }
                else
                {

                    var item = fileListBox.SelectedItem;
                    await Task.Run(() => playTrack((songItem)item));
                }
            }

        private async void ChDir_BTN_Click(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = Directory.GetCurrentDirectory();
            dialog.IsFolderPicker = true;
            var path = dialog.ShowDialog();
            if(path == CommonFileDialogResult.Ok) dirBox.Text = dialog.FileName;
            Directory.SetCurrentDirectory(dirBox.Text);
             scanFiles(extnBox.Text);
        }

        private void StpBTN_Click(object sender, RoutedEventArgs e)
        {
            isStopped = true;
            output.Stop();
        }

        private void Skip_BTN_Click(object sender, RoutedEventArgs e)
        {
            output.Stop();
        }

        private void SingleRDB_Checked(object sender, RoutedEventArgs e)
        {
            single = true;
            skip_BTN.IsEnabled = false;
        }

        private void AllRDB_Checked(object sender, RoutedEventArgs e)
        {
            single = false;
            skip_BTN.IsEnabled = true;
        }

        private void PropViewBTN_Click(object sender, RoutedEventArgs e)
        {
                    songItem item = (songItem)fileListBox.SelectedItem;
                    PropertiesView view = new PropertiesView(item);
                    view.Show();
                
        }

        private void PauseBTN_Click(object sender, RoutedEventArgs e)
        {
            pauseBTN.IsEnabled = false;
            playBTN.IsEnabled = true;
            isPaused = true;
            output.Pause();

        }

        private void Settings_Btn_Click(object sender, RoutedEventArgs e)
        {
            Settings view = new Settings();
            view.Show();
        }

        private void outBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            output.DeviceNumber = outBox.SelectedIndex - 1;
            Properties.Settings.Default.lastOutput = outBox.SelectedIndex;
            Properties.Settings.Default.Save();
        }

        private void extnBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (extnBox.Text.Contains("TextBox")) { return; }
            Properties.Settings.Default.lastFormat = extnBox.Text;
            Properties.Settings.Default.Save();
            System.Diagnostics.Debug.WriteLine(Properties.Settings.Default.lastFormat);
            scanFiles("."+extnBox.Text);
        }

        private async void fileListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            output.Stop();
            System.Threading.Thread.Sleep(500);
            playControls();
        }
    }
}