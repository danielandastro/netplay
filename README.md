Netplay is a lightweight audio player made in c#/WPF

Netplay utilises the NAudio library to play audio files

Netplay utilises the TagLib Sharp library to read ID3 Tags from audio files

It should run on windows Vista, although most formats require Windows 7 and above, I develop on Windows 10 x64, so please let me know
of any bugs, and include your OS and service pack. To send a bug report me at email
 incoming+danielandastro-netplay-13485622-issue-@incoming.gitlab.com

